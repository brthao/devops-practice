# create application load balancer
resource "aws_lb" "alb" {
  name               = "glady-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]
}

