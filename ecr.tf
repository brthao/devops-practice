
resource "aws_ecr_repository" "glady_ecr_repo" {
  name = "glady_ecr_repo"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_vpc_endpoint" "ecr_endpoint" {
  vpc_id            = aws_vpc.glady_vpc.id
  service_name      = "com.amazonaws.us-east-1.ecr.dkr"
  vpc_endpoint_type = "Interface"

  security_group_ids = [aws_security_group.ecr_sg.id]
  subnet_ids         = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]

  private_dns_enabled = true
}


resource "aws_security_group" "ecr_sg" {
  name_prefix = "ecr-sg"
  vpc_id      = aws_vpc.glady_vpc.id
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.glady_vpc.cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_ecr_repository_policy" "ecr_policy" {
  repository = aws_ecr_repository.glady_ecr_repo.name
  policy = jsonencode({
    Version = "2008-10-17",
    Statement = [
      {
        Effect = "Allow",
        Principal = "*",
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:BatchCheckLayerAvailability",
          "ecr:PutImage",
          "ecr:InitiateLayerUpload",
          "ecr:UploadLayerPart",
          "ecr:CompleteLayerUpload"
        ],
        Condition = {
          StringEquals = {
            "aws:sourceVpce" = aws_vpc_endpoint.ecr_endpoint.id
          }
        }
      }
    ]
  })
}
