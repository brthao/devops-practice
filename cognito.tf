

# create a VPC endpoint for Cognito
resource "aws_vpc_endpoint" "cognito_endpoint" {
  vpc_id = aws_vpc.glady_vpc.id
  service_name = "com.amazonaws.us-east-1.cognito-idp"
  vpc_endpoint_type = "Interface"

  security_group_ids = [aws_security_group.cognito_security_group.id]
  subnet_ids = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]

  private_dns_enabled = true

  tags = {
    Name = "cognito_endpoint"
  }
}

resource "aws_security_group" "cognito_security_group" {
  name_prefix = "cognito-security-group"
  vpc_id = aws_vpc.glady_vpc.id

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [aws_vpc.glady_vpc.cidr_block]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# not ended