# Glady exercise attempt by Bryan THAO

### Schema

![alt text](schema.png "AWS infra")

### Stack AWS :

- EKS :
    - déploiement d'un cluster kubernetes
- EC2 :
    - machines virtuelles utilisées par EKS
- Cognito :
    - gestion des utilisateurs (vos clients)
- RDS :
    - hébergement de BDD (postgres)
- ECR :
    - conteneur d'images docker (contiendra les images de nos micro-services)
- En plus :
    - MSK :
        - on peut imaginer utiliser MSK (kafka) pour la communication de nos micro-services back-end,
        - et pourquoi pas aussi AWS Glue schema registry pour gérer les versions de nos messages (protobuf).

ps : à noter que seulement l'ECR sera
multi-env (dev, sta, preprod, prod)


ps: network and eks part were tested but not the rest (not free)


## Details

#### 1. Main process

Hypothesis : the exercise screenshot is a view from a front micro-service (front-1).

Using the searchbar sends a request to a backend micro-service (backend-1).

At this instant, we're already inside our cluster. front-1 communicates with backend-1 (ex : req -> GET
/api/brand/search[...]).

When backend-1 process the request, it needs to know which client (user) is searching.

We can imagine passing the information in the request. Using a JWT, filled with some user informations.

Now that the backend knows which client, it can save the information in a database.

Let's use a postgres database (hosted with AWS RDS).

Example of row saved : (uuid, search_timestamp, cognito_user_id, [...])

The request can now return the desired search results.

### 2. Insight of the data

To get some insights, let's say we create an internal front-end micro service that will explore the data collected
previously.
We can choose to display it however we like, graphics, tab...

