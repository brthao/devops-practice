
# create vpc where everything will be for this exercise
resource "aws_vpc" "glady_vpc" {
  cidr_block = "10.0.0.0/16"
}

# create some public subnet
resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.glady_vpc.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
}

# create some public subnet
resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.glady_vpc.id
  cidr_block              = "10.0.4.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true
}

# create some private subnet
resource "aws_subnet" "private_subnet_1" {
  vpc_id            = aws_vpc.glady_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"
}

# create some private subnet
resource "aws_subnet" "private_subnet_2" {
  vpc_id            = aws_vpc.glady_vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1c"
}


# create gateway
resource "aws_internet_gateway" "glady_igw" {
  vpc_id = aws_vpc.glady_vpc.id
}

# attach gateway to vpc
resource "aws_internet_gateway_attachment" "glady_vpc_attachment" {
  vpc_id              = aws_vpc.glady_vpc.id
  internet_gateway_id = aws_internet_gateway.glady_igw.id
}