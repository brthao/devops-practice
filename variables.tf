variable "project" {
  type        = string
  description = "Project name"
  default     = "glady_exercise"
}
