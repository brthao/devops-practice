# create security group for eks
resource "aws_security_group" "eks_sg" {
  name_prefix = "eks_sg"
  vpc_id      = aws_vpc.glady_vpc.id
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# eks creation, working but not tested on long time cause not free, same for ec2
resource "aws_eks_cluster" "glady_eks" {
  name     = "glady_eks"
  role_arn = aws_iam_role.eks_iam_role.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.private_subnet_1.id,
      aws_subnet.private_subnet_2.id,
    ]
  }
}

# create role for eks
resource "aws_iam_role" "eks_iam_role" {
  name               = "eks_iam_role"
  path               = "/"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# allow role to create cluster
resource "aws_iam_role_policy_attachment" "eks-cluster-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_iam_role.name
}


resource "aws_eks_node_group" "this" {
  cluster_name    = aws_eks_cluster.glady_eks.id
  node_group_name = "worker-pool"
  node_role_arn   = aws_iam_role.eks_iam_role.arn
  subnet_ids      = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]
  instance_types  = ["m6i.large"]

  scaling_config {
    desired_size = 1
    max_size     = 6
    min_size     = 1
  }

  depends_on = [
    aws_eks_cluster.glady_eks
  ]

  lifecycle {
    ignore_changes = [
      scaling_config[0].desired_size
    ]
  }
}


