
# associate to subnets
resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "db-subnet"
  subnet_ids = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]
}

resource "aws_security_group" "rds_sg" {
  name_prefix = "rds_sg"
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# minimalist postgres db
resource "aws_db_instance" "glady_rds_instance" {
  identifier              = "glady-rds-instance"
  engine                  = "postgres"
  engine_version          = "14.4"
  instance_class          = "db.t2.micro"
  db_name                 = "my_db"
  username                = "my_user"
  password                = "my_password"
  storage_type            = "gp3"
  allocated_storage       = 20
  backup_retention_period = 7
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.rds_sg.id]
  db_subnet_group_name    = aws_db_subnet_group.rds_subnet_group.name
}
