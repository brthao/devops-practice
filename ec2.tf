
# authorize ec2 to assume this role and act in eks
resource "aws_iam_role" "eks_worker_nodes_role" {
  name               = "eks-worker-nodes"
  assume_role_policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# create an IAM instance profile for the worker nodes
resource "aws_iam_instance_profile" "eks_worker_nodes_instance_profile" {
  name = "eks-worker-nodes"
  role = aws_iam_role.eks_worker_nodes_role.id
}

# some sg that allows incoming ssh from any ip
resource "aws_security_group" "vm0_sg" {
  name_prefix = "vm0_sg"
  vpc_id      = aws_vpc.glady_vpc.id
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_iam_instance_profile_association" "vm0_sg_profile_association" {
  instance_profile_arn = aws_iam_instance_profile.eks_worker_nodes_instance_profile.arn
  name                 = "vm0_sg_profile_association"
  resources            = [aws_security_group.vm0_sg.id]
}

# create an ec2 virtual machine
resource "aws_instance" "vm0" {
  ami                    = "ami-006dcf34c09e50022"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.private_subnet_1.id
  vpc_security_group_ids = [aws_security_group.vm0_sg.id]
  iam_instance_profile   = aws_iam_instance_profile.eks_worker_nodes_instance_profile.name
}